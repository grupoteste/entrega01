---
title: "Problema 1 - P vs. NP"
date: 2021-10-24T12:03:24-03:00
draft: false
---

O problema "P versus NP" é o principal problema aberto da Ciência da Computação. Possui também enorme relevância em campos que vão desde a Engenharia até a criptografia aplicada aos serviços militares e às transações comerciais e financeiras via Internet.

Atividade proposta pelo Instituto Clay é a seguinte: você precisa organizar as acomodações de um grupo de 400 estudantes universitários, mas apenas 100 estudantes receberão lugares no dormitório, pois não há espaço para todos. Para complicar, o reitor lhe forneceu uma lista de pares de estudantes que não podem ficar juntos. Diz o regulamento do prêmio do milênio: “este é um exemplo que os cientistas denominam uma NP-problema, uma vez que é fácil verificar se uma dada escolha de 100 estudantes proposta é satisfatória (isto é, verificar se nenhum par da lista pronta aparece na lista do reitor), porém a tarefa de gerar uma lista desse tipo a partir do zero parece ser tão difícil quanto completamente impraticável”. Ou seja, é possível checar uma lista de cada vez, mas não se chegou a um cálculo que garanta que o resultado final contemple os dois critérios. Só pode ser programado se incluir todos os casos. 