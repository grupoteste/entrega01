---
title: "Problema 2 - Conjectura de Hodge"
date: 2021-10-24T12:03:24-03:00
draft: false
---

A conjectura de Hodge é um importante problema, ainda não resolvido, de geometria algébrica no que diz respeito a topologias de variedade algébrica complexa não singular e as subvariedades dessa variedade. Concretamente, a conjectura propõe que certos grupos de co-homologia de Rham são algébricos, isto é, são somas de dualidades de Poincaré de classes homólogas de subvariedades.

A conjectura de Hodge é um dos Problemas do Milênio do Clay Mathematics Institute, cuja solução faz jus a um prêmio de US$1.000.000,00.