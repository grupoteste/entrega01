---
title: "Problema 6 - Existência e suavidade de Navier-Stokes"
date: 2021-10-24T12:03:24-03:00
draft: false
---

As Equações de Navier-Stokes são um dos pilares da mecânica de fluidos. Estas equações descrevem o movimento de um fluido (líquido ou gás) no espaço físico.

As soluções das equações de Navier-Stokes são utilizadas em diversas aplicações práticas, entretanto, o entendimento teórico destas soluções são incompletos. Particularmente, as soluções destas equações incluem turbulência, as quais se mantém como um dos maiores problemas em aberto da física, apesar de sua imensa importância para a física teórica e a engenharia.

Já que o completo entendimento das equações de Navier–Stokes é considerado o primeiro passo para o entendimento da turbulência em fluidos, o Clay Mathematics Institute ofereceu em maio de 2010 um prêmio de um milhão de dólares para qualquer pessoa que comprove estas equações e forneça uma pista para os fenômenos turbulentos. O desafio é explicado como um problema matemático concreto.[1]