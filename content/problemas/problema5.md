---
title: "Problema 5 - Existência de Yang-Mills e intervalo de massa"
date: 2021-10-24T12:03:24-03:00
draft: false
---

A comprovação da Existência de Yang-Mills e intervalo de massa é um dos problemas do millenium e continua sem solução. Este problema é um dos requisitos para a prova matemática da teoria quântica de campos de acordo com o modelo padrão de partículas fundamentais.

O problema consiste na prova com todo o rigor matemático característico da física matemática contemporânea. O vencedor também deve provar que a massa da menor partícula fundamental predita pela teoria quântica de campos seja positiva, ou seja, a partícula precisa possuir um intervalo de massa.