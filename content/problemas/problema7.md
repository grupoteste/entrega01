---
title: "Problema 7 - Conjectura de Birch e Swinnerton-Dyer"
date: 2021-10-24T12:03:05-03:00
draft: false
---
A conjectura de Birch e Swinerton-Dyer foi enunciada em 1965 e estabelece uma condição para que uma curva algébrica plana, f(x,y) = 0, definida sobre os racionais — isto é, com os argumentos x,y∈ℚ—, tenha infinitos pontos racionais —isto é, (x,y) solução de f(x,y) = 0, com x,y∈ℚ—, como por exemplo a circunferência.