---
title: "Problema 4 - Hipótese de Riemann"
date: 2021-10-24T12:03:24-03:00
draft: false
---
Em matemática, a hipótese de Riemann é uma conjectura de que a função zeta de Riemann tem os seus zeros somente nos números inteiros pares negativos e em números complexos com parte real  
1
2
. Muitos consideram que este é o problema não resolvido mais importante da matemática pura (Bombieri 2000). Ela é de grande interesse em teoria de números, porque implica resultados sobre a distribuição dos números primos. Ela foi proposta por Bernhard Riemann (1859), de quem recebe o nome.

A hipótese de Riemann e algumas de suas generalizações, juntamente com a conjectura de Goldbach e a conjectura dos primos gêmeos, compõem o oitavo problema na lista de 23 problemas não-resolvidos de David Hilbert; também é um dos Problemas do Prémio Millennium do Clay Mathematics Institute. O nome também é usado para alguns análogos intimamente relacionados, tais como a hipótese de Riemann para curvas definidas sobre corpos finitos.