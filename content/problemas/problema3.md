---
title: "Problema 3 - Conjectura de Poincaré"
date: 2021-10-24T12:03:24-03:00
draft: false
---
A conjectura de Poincaré afirma que qualquer variedade tridimensional fechada e com grupo fundamental trivial é homeomorfa a uma esfera tridimensional. Ou seja, a superfície tridimensional de uma esfera é o único espaço fechado de dimensão 3 onde todos os contornos ou caminhos podem ser encolhidos até chegarem a um simples ponto.

Esta conjectura surgiu na sequência de uma outra conjectura formulada por Henri Poincaré em 1900, que afirmava que qualquer variedade tridimensional fechada e com homologia trivial (denominada uma esfera de homologia) era homeomorfa a uma esfera. Na verdade esta conjectura foi refutada pelo próprio Poincaré em 1904, que forneceu o primeiro exemplo de uma esfera de homologia não homeomorfa a uma esfera.

Em 2003, o russo Grigori Perelman, anunciou uma solução positiva para o problema, recusando o prêmio Clay no valor de um milhão de dólares.