---
title: "Curiosidade - O caso de Grigory Perelman"
date: 2021-10-22T23:14:33-03:00
draft: false
---






Perelman, tido com excêntrico e recluso, solucionou a conjectura de Poincaré, problema de número 3, em artigos publicados na internet nos anos de 2002 e 2003.

Quando a solução do problema foi confirmada, em 2006, ele foi indicado para receber a Fields Medal – considerado o Nobel da matemática – mas recusou ao prêmio.

Na ocasião, o matemático afirmou que a medalha era irrelevante para ele e que o fato de a solução estar correta já seria reconhecimento suficiente.

Ele não compareceu à entrega da medalha, programada para ser feita pelo do Rei Juan Carlos, da Espanha, durante o Congresso Internacional de Matemáticos, em Madri, em 2006. O congresso é realizado a cada quatro anos.

A solução do problema também foi reconhecida como “Avanço do Ano” pela revista especializada Science, em 2006.

Antes disso, ele também tinha recusado um prêmio do Congresso Europeu de Matemáticos, em 1996.

A conjectura de Poincaré era um dos sete desafios levantados pela CMI para os chamados Prêmios do Milênio, lançados no ano 2000.

Os prêmios foram criados para chamar a atenção e recompensar a solução de alguns dos problemas mais difíceis enfrentados pelos matemáticos na virada do milênio. A conjectura de Poincaré foi o único problema solucionado até agora.

A conjectura de Poincaré foi formulada em 1904 pelo matemático francês Henri Poincaré e é de difícil compreensão para leigos e seria, segundo o CMI fundamental para se compreender formas tridimensionais.

Segundo a Wikipedia, a conjectura afirma que "qualquer variedade tridimensional fechada e com grupo fundamental trivial é homeomorfa a uma esfera tridimensional. Ou seja, num espaço com três dimensões fechado, sem 'buracos' deve ter a forma de uma esfera".